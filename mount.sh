docker run -d --name $1 -it \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.x11-unix:rw" \
  --volume="/home/$USER/data:/root/catkin_ws:ro" \
  --volume="/etc/group:/etc/group:ro" \
  --volume="/etc/passwd:/etc/passwd:ro" \
  --volume="/etc/shadow:/etc/shadow:ro" \
  --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
  --env QT_X11_NO_MITSHM=1 \
  ${2:-gtmobilemanipulation/mm8803-fetch-gazebo:latest}