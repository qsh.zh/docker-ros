docker run -it \
    --name $1 \
    --workdir="/root" \
    --env QT_X11_NO_MITSHM=1 \
    -p 5599:5588 \
    ${2:-local/melodic-base:latest}
