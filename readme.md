# Docker ros  
This repo contains some helpful scirpts for run ros in docker, based on melodic.
**ATTENTION!!! Make sure the SECURITY of your SSH KEY**
## ROS version
```shell
# need x11
FROM osrf/ros:melodic-desktop-full
# clean, base
FROM ros:melodic-ros-base-bionic
```

# How does it works 
[docker part and bootstrap](https://medium.com/dot-debug/running-chrome-in-a-docker-container-a55e7f4da4a8)  
**Remark**
- Because ros needs to communicate with the robots, network setting is a bit different. Use `network=host` to start a container, we do not need to set the port mapping
- Do not set the `DISPLAY` variable `:0`,`:1`, they may introduce conflict
- recommand use the `Remmina` do the local vnc client

## Build
```shell
docker build -t local/melodic-base .
```

# TODO 
- [ ] Can we directly set the ip address to the container? 
- [ ] Ros version details? 