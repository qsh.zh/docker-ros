# FROM ros:melodic-ros-base-bionic
FROM osrf/ros:melodic-desktop-full

# Install necessary packages
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    wget \
    curl \
    git \
    net-tools \
    unzip \
    vim \
    wget \
    tmux \
    ssh \
    zsh

# Install dotfiles
COPY .ssh/ /root/.ssh
RUN ls -al /root/
RUN chown root /root/.ssh/config
RUN cd ~ \
    && git clone --recursive git@gitlab.com:zqsh419/dotfiles.git \
    && cd dotfiles \
    && bash install

# Create ROS ws
RUN mkdir -p /root/catkin_ws/src \
    && cd /root/catkin_ws

# Install a VNC X-server, Frame buffer, and windows manager
RUN apt-get install -y x11vnc xvfb fluxbox

# Finally, install wmctrl needed for boostrap script
RUN apt-get install -y wmctrl 

# Copy bootstrap script and make sure it runs
COPY bootstrap.sh /

CMD '/bootstrap.sh'
